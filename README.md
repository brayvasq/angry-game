# Angry Things Game

[toc]

## Creación del proyecto

| Herramienta | Version | Descripción                                                  |
| ----------- | ------- | ------------------------------------------------------------ |
| Java        | 11.0.6  | Lenguaje de programación                                     |
| Maven       | 3.6.0   | Herramienta para la gestión y creación de proyectos JAVA y con una configuración basada en xml |

```bash
# Version de maven
mvn --verison

# Crear proyecto
mvn archetype:generate -DgroupId=com.brayvasq.AngryGame -DartifactId=AngryGame -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

# Compilar proyecto
mvn compile

# Crear jar
mvn clean package

# Ejecutar proyecto
java -cp target/AngryGame-1.0-SNAPSHOT.jar com.brayvasq.AngryGame.App

# Ejecutar tests
mvn test

# Ejecutar proyecto con maven (Es necesario el plugin 'exec-maven-plugin')
mvn exec:java

# Ver dependencias 
mvn dependency:tree -Dverbose
```

### Dependencias

Añadir las siguientes dependencias al archivo `pom.xml`

- `maven-compiler-plugin` - Para la compilación del proyecto.
- `exec-maven-plugin` - Para ejecutar el proyecto usando mvn

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.brayvasq.AngryGame</groupId>
  <artifactId>AngryGame</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>AngryGame</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <build> <!-- Needed for build a project -->
    <plugins>
      <plugin> <!-- To compile project -->
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
          <configuration>
            <source>1.8</source>
            <target>1.8</target>
          </configuration>
      </plugin>
      <plugin> <!-- To exec project using mvn exec:java -->
        <groupId>org.codehaus.mojo</groupId>
          <artifactId>exec-maven-plugin</artifactId>
            <version>1.2.1</version>
            <configuration>
              <mainClass>com.brayvasq.AngryGame.App</mainClass>
            </configuration>
      </plugin>
    </plugins>
  </build>
</project>
```

## Creando Frame

Crear el paquete `views`, quedando la siguiente estructura `com.brayvasq.AngryGame.views`. 

Crear el archivo `Frame.java` en el paquete `views`, quedando la estructura así:

```bash
src/
├── main
│   └── java
│       └── com
│           └── brayvasq
│               └── AngryGame
│                   ├── App.java
│                   └── views
│                       └── Frame.java
└── test
```

Archivo `Frame.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author brayvasq
 */
public class Frame extends JFrame{
    
    public void setup(){
        // Set window size
        this.setSize(new Dimension(800,400));
        // Allow to close the window
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // To center the window in the screen
        this.setLocationRelativeTo(null);
        // To not allow resize the window
        this.setResizable(false);
        // To show the window
        this.setVisible(true);
    }
}
```

Archivo `App.java`

```java
package com.brayvasq.AngryGame;

import com.brayvasq.AngryGame.views.Frame;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Frame frame = new Frame();
    }
}
```

Ejecutar el proyecto

```bash
mvn exec:java
```

## Creando Panel

Crear el archivo `Panel.java` en el paquete `views`, quedando la estructura así:

```bash
src/
├── main
│   └── java
│       └── com
│           └── brayvasq
│               └── AngryGame
│                   ├── App.java
│                   └── views
│                       ├── Frame.java
│                       └── Panel.java
└── test
```

Archivo `Panel.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

/**
 *
 * @author brayvasq
 */
public class Panel extends JPanel{
    private Color primary;
    private Color secondary;
    
    public Panel(){
        // Setup colors
        this.primary = new Color(52,73,94);
        this.secondary = new Color(236, 240, 241);
        
        // To remove default panel layout
        this.setLayout(null);
        // To set the panel size
        this.setSize(new Dimension(600,300));
        // Change the panel background color
        this.setBackground(this.primary);
    }
}
```

Archivo `Frame.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author brayvasq
 */
public class Frame extends JFrame{
    
    private Panel panel;
    
    public Frame(){
        ....
        // Setup panel
        this.configPanel();     
        // To show the window
        this.setVisible(true);
    }
    
    public void configPanel(){
        this.panel = new Panel();
        this.add(this.panel, BorderLayout.CENTER);
    }
}
```

## Creando Jugador

Crear el paquete `models`, quedando la siguiente estructura `com.brayvasq.AngryGame.models`. 

Crear el archivo `Player.java` en el paquete `models`, quedando la estructura así:

```bash
src/
├── main
│   └── java
│       └── com
│           └── brayvasq
│               └── AngryGame
│                   ├── App.java
│                   ├── models
│                   │   └── Player.java
│                   └── views
│                       ├── Frame.java
│                       └── Panel.java
└── test
```

Archivo `Player.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.models;

import java.awt.Rectangle;
import java.util.ArrayList;

/**
 *
 * @author brayvasq
 */
public class Player {
    // Y axis
    private double y;   //  Y position
    private double y0;  //  Initial Y position
    private double vy0; //  Initial Y speed
    
    // X axis
    private double x;   //  X position
    private double x0;  //  Initial X position
    private double vx0; //  Initial X speed;
    
    // Engine variables
    private double initialSpeed;
    private double time;
    private int angle;
    private ArrayList<Integer> ys = new ArrayList<>(); // Used to store the Y points around the time
    private ArrayList<Integer> xs = new ArrayList<>(); // Used to store the X points around the time
    private double fx;  //  X force
    private double fy = 0; //   Y force
    
    // Player variables
    private boolean canShoot = true;
    private int width;
    private int height;
    
    /**
     * Empty constructor.
     */
    public Player(){}
    
    /**
     * Player constructor.
     * @param width player width
     * @param height player height
     */
    public Player(int width, int height){
        this.setupVariables(0,0);
        this.width = width;
        this.height = height;
    }
    
    /**
     * Player constructor.
     * @param x player horizontal position
     * @param y player vertical position
     * @param width player horizontal size
     * @param height player vertical size
     */
    public Player(double x, double y, int width, int height){
        this.setupVariables((int)x, (int)y);
        this.width = width;
        this.height = height;
    }
    
    /**
     * Surround the player's area with a rectangle, used to 
     * identify collisions.
     * @return Rectangle surrounded the player's area
     */
    public Rectangle getBounds(){
        return new Rectangle((int)this.x, (int)this.y, this.width, this.height);
    }
    
    /**
     * Setup the position and initial speed variables.
     * Equations used:
     *      Y axis -> v0 = v0 * sen(θ)
     *      X axis -> v0 = v0 * cos(θ)
     */
    public void setupVariables(int x, int y){
        this.y = y;
        this.y0 = y;
        System.out.println("[Setup] Y : "+this.y);
        System.out.println("[Setup] Y0 : "+this.y0);
        
        System.out.println("[Setup] Angle : "+this.angle);
        System.out.println("[Setup] Radians:" + Math.toRadians(this.angle) + " SIN: "+Math.sin(Math.toRadians(this.angle)));
        System.out.println("[Setup] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.cos(Math.toRadians(this.angle)));
        // Equation: v0 = v0 * sen(θ)
        this.vy0 = this.initialSpeed * Math.sin(Math.toRadians(this.angle));
        // Equation: v0 = v0 * cos(θ)
        this.vx0 = this.initialSpeed * Math.cos(Math.toRadians(this.angle));
        System.out.println("[Setup] VY0 : "+this.vy0);
        System.out.println("[Setup] VX0 : "+this.vx0);
        
        
        this.x = x;
        this.x0 = x;
        System.out.println("[Setup] X : "+this.x);
        System.out.println("[Setup] X0 : "+this.x0);
        
        this.time = 0;
    }
    
    /**
     * Method that updates the player position and speed.
     * synchronized -> allows the method and variables related to be 
     * acceced only by one process
     */
    public synchronized void update(){
        // Update time and new position
        double timeIncrement = 0.05;
        this.time += timeIncrement;
        
        System.out.println("[Update] Time : "+this.time);
        System.out.println("[Update] Angle : "+this.angle);
        System.out.println("[Update] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.cos(Math.toRadians(this.angle)));
        // Equation: x = v0 * cos(θ) * t
        x = this.vx0 * Math.cos(Math.toRadians(this.angle)) * this.time;
        System.out.println("[Update] VX0 : "+this.vx0);
        System.out.println("[Update] X : "+this.x);
        System.out.println("[Update] X0 : "+this.x0);
        
        // Gravity value
        double g = -9.81;
        System.out.println("[Update] Gravity : "+g);
        System.out.println("[Update] Angle : "+this.angle);
        System.out.println("[Update] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.sin(Math.toRadians(this.angle)));


        // Equation: y(t) = v0 * sen(θ) * t - .5 * g * t2
        double tempY = this.vy0 * Math.sin(Math.toRadians(this.angle)) * this.time + (0.5 * g * this.time * this.time);
        System.out.println("[Update] VY0 : "+this.vy0);
        System.out.println("[Update] TEMP Y : "+tempY);
        System.out.println("[Update] Y0 : "+this.y0);
        this.y = this.y0 - tempY;
        System.out.println("[Update] Y : "+this.y);
        
        this.ys.add((int) this.y);
        this.xs.add((int) this.x);
    }
    
    /**
     * Clean the values ​​of the lists and initialize the variables again.
     */
    public void clearVariables(){
        System.out.println("[Clear] Y0 : "+this.y0);
        System.out.println("[Clear] X0 : "+this.x0);
        this.setupVariables((int)this.x0, (int)this.y0);
        this.xs.removeAll(this.xs);
        this.ys.removeAll(this.ys);
        this.fx = 0;
        this.fy = 0;
    }
    
    // Getters and Setters
}
```

## Dibujando en el Panel

Archivo `Panel.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import com.brayvasq.AngryGame.models.Player;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Collections;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author brayvasq
 */
public class Panel extends JPanel implements Runnable{
    private Color primary;
    private Color secondary;
    private Player p1, p2;
    private int status;
    
    public Panel(){
        // Setup colors
        this.primary = new Color(52,73,94);
        this.secondary = new Color(236, 240, 241);
        
        // To remove default panel layout
        this.setLayout(null);
        // To set the panel size
        this.setSize(new Dimension(600,300));
        // Change the panel background color
        this.setBackground(this.primary);
        
        System.out.println("[Panel] Height : "+this.getHeight());
        System.out.println("[Panel] Width : "+this.getWidth());
        
        this.p1 = new Player(10, 60, 60,60);
        this.p2 = new Player(this.getWidth() - 10, this.getHeight() - 10,60,60);
        this.status = this.p2.getWidth();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.

        if(this.p1 != null) {
            //this.setOpaque(false);
            g.setColor(secondary);
            
            g.drawRect((int) this.p1.getX(), (int) this.p1.getY(), 30, 30);
            
            if(this.p2 != null){
                g.drawRect((int) this.p2.getX(), (int) this.p2.getY(), 30, 30);
                
                g.setColor(Color.RED);
                g.drawRect((int) this.p2.getX(), (int) this.p2.getY()- 20, this.p2.getWidth()+1, 11);
                
                g.setColor(Color.YELLOW);
                g.fillRect((int) this.p2.getX() + 1, (int) this.p2.getY() - 19, this.status, 11);
            }
            
            for (int i = 0; i < this.p1.getXs().size(); i++) {
                if( i % 8 == 0){
                    g.drawOval(this.p1.getXs().get(i), this.p1.getYs().get(i), 5, 5);
                }
            }
            
            if(this.p1.getBounds().intersects(this.p2.getBounds())){
                if(this.status == 0){
                    JOptionPane.showMessageDialog(null, "You Won!");
                }else{
                    this.status -= 1;
                }
            }
        }
    }

    /**
     * 
     */
    public void shoot(){
        if(this.p1.isCanShoot()){
            new Thread(this).start();
        }
    }
    
    public void changeValues(int angle, int speed){
        this.p1.setAngle(angle);
        this.p1.setInitialSpeed(speed);
        this.p1.setupVariables((int) this.p1.getX0(),(int) this.p1.getY0());
    }
    
    @Override
    public void run() {
        this.p1.setCanShoot(false);
        
        while(this.p1.getY() <= this.getHeight() && this.status > 0){
            try {
                
                this.p1.update();
                Thread.sleep(20);
                this.repaint();
                
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        if(this.status <= 0){
            this.status = this.p2.getWidth();
        }
        
        this.p1.setFx(Collections.max(this.p1.getXs()) / 2);
        this.p1.setFy(Collections.min(this.p1.getYs()));
        
        this.p1.clearVariables();
        this.repaint();
        this.p1.setCanShoot(true);
    }

    /**
     * @return the p1
     */
    public Player getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(Player p1) {
        this.p1 = p1;
    }
    
}
```

Archivo `Frame.java`

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import com.brayvasq.AngryGame.models.Player;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author brayvasq
 */
public class Frame extends JFrame{
    
    private Panel panel;
    private JPanel acctions;
    private Thread t;
    private JTextField txtAngle, txtSpeed;
    private JLabel lblAngle, lblSpeed;
    private JButton btnShoot;
    
    public Frame(){
        // Set window size
        this.setSize(new Dimension(800,400));
        // Allow to close the window
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // To center the window in the screen
        this.setLocationRelativeTo(null);
        // Setup panel actions
        this.configActions();
        // Setup panel game
        this.configPanel();     
        // To not allow resize the window
        this.setResizable(false);
        // To show the window
        this.setVisible(true);
    }
    
    public void configPanel(){
        this.panel = new Panel();
        this.add(this.panel, BorderLayout.CENTER);
        this.t = new Thread(this.panel);
    }
    
    public void configActions(){
        this.acctions = new JPanel();
        
        this.btnShoot = new JButton("Shoot");
        this.btnShoot.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    int angle = Integer.parseInt(txtAngle.getText());
                    int speed = Integer.parseInt(txtSpeed.getText());
                    
                    if(angle > 90 || angle <= 0){
                        JOptionPane.showMessageDialog(null, "Angle Error: should be > 0 and <= 90");
                    }else if(speed <= 0) {
                        JOptionPane.showMessageDialog(null, "Speed Error: should be > 0");
                    }else {
                        panel.changeValues(angle, speed);
                        panel.shoot();
                    }
                } catch(java.lang.NumberFormatException e){
                    System.out.println(e.toString());
                    JOptionPane.showMessageDialog(null, "Speed and Angle cannot be empty");
                } catch(NullPointerException e){
                    System.out.println(e.toString());
                }
                
            } 
        });
        
        this.txtAngle = new JTextField("");
        this.txtSpeed = new JTextField("");
        
        this.lblAngle = new JLabel("Angle");
        this.lblSpeed = new JLabel("Speed");
        
        this.txtAngle.setPreferredSize(new Dimension( 50, 25 ));
        this.txtSpeed.setPreferredSize(new Dimension( 50, 25 ));
        
        this.acctions.add(this.btnShoot, BorderLayout.EAST);
        this.acctions.add(this.lblAngle, BorderLayout.WEST);
        this.acctions.add(this.txtAngle, BorderLayout.WEST);
        this.acctions.add(this.lblSpeed, BorderLayout.WEST);
        this.acctions.add(this.txtSpeed, BorderLayout.WEST);
        
        this.add(this.acctions, BorderLayout.NORTH);
    }
}
```

Final structure

```bash
src/
├── main
│   └── java
│       └── com
│           └── brayvasq
│               └── AngryGame
│                   ├── App.java
│                   ├── models
│                   │   └── Player.java
│                   └── views
│                       ├── Frame.java
│                       └── Panel.java
└── test
```

