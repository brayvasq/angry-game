/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.models;

import java.awt.Rectangle;
import java.util.ArrayList;

/**
 *
 * @author brayvasq
 */
public class Player {
    // Y axis
    private double y;   //  Y position
    private double y0;  //  Initial Y position
    private double vy0; //  Initial Y speed
    
    // X axis
    private double x;   //  X position
    private double x0;  //  Initial X position
    private double vx0; //  Initial X speed;
    
    // Engine variables
    private double initialSpeed;
    private double time;
    private int angle;
    private ArrayList<Integer> ys = new ArrayList<>(); // Used to store the Y points around the time
    private ArrayList<Integer> xs = new ArrayList<>(); // Used to store the X points around the time
    private double fx;  //  X force
    private double fy = 0; //   Y force
    
    // Player variables
    private boolean canShoot = true;
    private int width;
    private int height;
    
    /**
     * Empty constructor.
     */
    public Player(){}
    
    /**
     * Player constructor.
     * @param width player width
     * @param height player height
     */
    public Player(int width, int height){
        this.setupVariables(0,0);
        this.width = width;
        this.height = height;
    }
    
    /**
     * Player constructor.
     * @param x player horizontal position
     * @param y player vertical position
     * @param width player horizontal size
     * @param height player vertical size
     */
    public Player(double x, double y, int width, int height){
        this.setupVariables((int)x, (int)y);
        this.width = width;
        this.height = height;
    }
    
    /**
     * Surround the player's area with a rectangle, used to 
     * identify collisions.
     * @return Rectangle surrounded the player's area
     */
    public Rectangle getBounds(){
        return new Rectangle((int)this.x, (int)this.y, this.width, this.height);
    }
    
    /**
     * Setup the position and initial speed variables.
     * Equations used:
     *      Y axis -> v0 = v0 * sen(θ)
     *      X axis -> v0 = v0 * cos(θ)
     */
    public void setupVariables(int x, int y){
        this.y = y;
        this.y0 = y;
        System.out.println("[Setup] Y : "+this.y);
        System.out.println("[Setup] Y0 : "+this.y0);
        
        System.out.println("[Setup] Angle : "+this.angle);
        System.out.println("[Setup] Radians:" + Math.toRadians(this.angle) + " SIN: "+Math.sin(Math.toRadians(this.angle)));
        System.out.println("[Setup] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.cos(Math.toRadians(this.angle)));
        // Equation: v0 = v0 * sen(θ)
        this.vy0 = this.initialSpeed * Math.sin(Math.toRadians(this.angle));
        // Equation: v0 = v0 * cos(θ)
        this.vx0 = this.initialSpeed * Math.cos(Math.toRadians(this.angle));
        System.out.println("[Setup] VY0 : "+this.vy0);
        System.out.println("[Setup] VX0 : "+this.vx0);
        
        
        this.x = x;
        this.x0 = x;
        System.out.println("[Setup] X : "+this.x);
        System.out.println("[Setup] X0 : "+this.x0);
        
        this.time = 0;
    }
    
    /**
     * Method that updates the player position and speed.
     * synchronized -> allows the method and variables related to be 
     * acceced only by one process
     */
    public synchronized void update(){
        // Update time and new position
        double timeIncrement = 0.05;
        this.time += timeIncrement;
        
        System.out.println("[Update] Time : "+this.time);
        System.out.println("[Update] Angle : "+this.angle);
        System.out.println("[Update] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.cos(Math.toRadians(this.angle)));
        // Equation: x = v0 * cos(θ) * t
        x = this.vx0 * Math.cos(Math.toRadians(this.angle)) * this.time;
        System.out.println("[Update] VX0 : "+this.vx0);
        System.out.println("[Update] X : "+this.x);
        System.out.println("[Update] X0 : "+this.x0);
        
        // Gravity value
        double g = -9.81;
        System.out.println("[Update] Gravity : "+g);
        System.out.println("[Update] Angle : "+this.angle);
        System.out.println("[Update] Radians:" + Math.toRadians(this.angle) + " COS: "+Math.sin(Math.toRadians(this.angle)));


        // Equation: y(t) = v0 * sen(θ) * t - .5 * g * t2
        double tempY = this.vy0 * Math.sin(Math.toRadians(this.angle)) * this.time + (0.5 * g * this.time * this.time);
        System.out.println("[Update] VY0 : "+this.vy0);
        System.out.println("[Update] TEMP Y : "+tempY);
        System.out.println("[Update] Y0 : "+this.y0);
        this.y = this.y0 - tempY;
        System.out.println("[Update] Y : "+this.y);
        
        this.ys.add((int) this.y);
        this.xs.add((int) this.x);
    }
    
    /**
     * Clean the values ​​of the lists and initialize the variables again.
     */
    public void clearVariables(){
        System.out.println("[Clear] Y0 : "+this.y0);
        System.out.println("[Clear] X0 : "+this.x0);
        this.setupVariables((int)this.x0, (int)this.y0);
        this.xs.removeAll(this.xs);
        this.ys.removeAll(this.ys);
        this.fx = 0;
        this.fy = 0;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the y0
     */
    public double getY0() {
        return y0;
    }

    /**
     * @param y0 the y0 to set
     */
    public void setY0(double y0) {
        this.y0 = y0;
    }

    /**
     * @return the vy0
     */
    public double getVy0() {
        return vy0;
    }

    /**
     * @param vy0 the vy0 to set
     */
    public void setVy0(double vy0) {
        this.vy0 = vy0;
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the x0
     */
    public double getX0() {
        return x0;
    }

    /**
     * @param x0 the x0 to set
     */
    public void setX0(double x0) {
        this.x0 = x0;
    }

    /**
     * @return the vx0
     */
    public double getVx0() {
        return vx0;
    }

    /**
     * @param vx0 the vx0 to set
     */
    public void setVx0(double vx0) {
        this.vx0 = vx0;
    }

    /**
     * @return the initialSpeed
     */
    public double getInitialSpeed() {
        System.out.println("[GET] Initial Speed : "+this.initialSpeed);
        return initialSpeed;
    }

    /**
     * @param initialSpeed the initialSpeed to set
     */
    public void setInitialSpeed(double initialSpeed) {
        System.out.println("[BEFORE SET] Initial Speed : "+this.initialSpeed);
        this.initialSpeed = initialSpeed;
        System.out.println("[AFTER SET] Initial Speed : "+this.initialSpeed);
    }

    /**
     * @return the time
     */
    public double getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(double time) {
        this.time = time;
    }

    /**
     * @return the angle
     */
    public int getAngle() {
        System.out.println("[GET] Angle : "+this.angle);
        return angle;
    }

    /**
     * @param angle the angle to set
     */
    public void setAngle(int angle) {
        System.out.println("[BEFORE SET] Angle : "+this.angle);
        this.angle = angle;
        System.out.println("[AFTER SET] Angle : "+this.angle);
    }

    /**
     * @return the ys
     */
    public ArrayList<Integer> getYs() {
        return ys;
    }

    /**
     * @param ys the ys to set
     */
    public void setYs(ArrayList<Integer> ys) {
        this.ys = ys;
    }

    /**
     * @return the xs
     */
    public ArrayList<Integer> getXs() {
        return xs;
    }

    /**
     * @param xs the xs to set
     */
    public void setXs(ArrayList<Integer> xs) {
        this.xs = xs;
    }

    /**
     * @return the fx
     */
    public double getFx() {
        return fx;
    }

    /**
     * @param fx the fx to set
     */
    public void setFx(double fx) {
        this.fx = fx;
    }

    /**
     * @return the fy
     */
    public double getFy() {
        return fy;
    }

    /**
     * @param fy the fy to set
     */
    public void setFy(double fy) {
        this.fy = fy;
    }

    /**
     * @return the canShoot
     */
    public boolean isCanShoot() {
        return canShoot;
    }

    /**
     * @param canShoot the canShoot to set
     */
    public void setCanShoot(boolean canShoot) {
        this.canShoot = canShoot;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
