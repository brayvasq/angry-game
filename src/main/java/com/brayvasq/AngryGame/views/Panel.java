/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import com.brayvasq.AngryGame.models.Player;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Collections;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author brayvasq
 */
public class Panel extends JPanel implements Runnable{
    private Color primary;
    private Color secondary;
    private Player p1, p2;
    private int status;
    
    public Panel(){
        // Setup colors
        this.primary = new Color(52,73,94);
        this.secondary = new Color(236, 240, 241);
        
        // To remove default panel layout
        this.setLayout(null);
        // To set the panel size
        this.setSize(new Dimension(600,300));
        // Change the panel background color
        this.setBackground(this.primary);
        
        System.out.println("[Panel] Height : "+this.getHeight());
        System.out.println("[Panel] Width : "+this.getWidth());
        
        this.p1 = new Player(10, 60, 60,60);
        this.p2 = new Player(this.getWidth() - 10, this.getHeight() - 10,60,60);
        this.status = this.p2.getWidth();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.

        if(this.p1 != null) {
            //this.setOpaque(false);
            g.setColor(secondary);
            
            g.drawRect((int) this.p1.getX(), (int) this.p1.getY(), 30, 30);
            
            if(this.p2 != null){
                g.drawRect((int) this.p2.getX(), (int) this.p2.getY(), 30, 30);
                
                g.setColor(Color.RED);
                g.drawRect((int) this.p2.getX(), (int) this.p2.getY()- 20, this.p2.getWidth()+1, 11);
                
                g.setColor(Color.YELLOW);
                g.fillRect((int) this.p2.getX() + 1, (int) this.p2.getY() - 19, this.status, 11);
            }
            
            for (int i = 0; i < this.p1.getXs().size(); i++) {
                if( i % 8 == 0){
                    g.drawOval(this.p1.getXs().get(i), this.p1.getYs().get(i), 5, 5);
                }
            }
            
            if(this.p1.getBounds().intersects(this.p2.getBounds())){
                if(this.status == 0){
                    JOptionPane.showMessageDialog(null, "You Won!");
                }else{
                    this.status -= 1;
                }
            }
        }
    }

    /**
     * 
     */
    public void shoot(){
        if(this.p1.isCanShoot()){
            new Thread(this).start();
        }
    }
    
    public void changeValues(int angle, int speed){
        this.p1.setAngle(angle);
        this.p1.setInitialSpeed(speed);
        this.p1.setupVariables((int) this.p1.getX0(),(int) this.p1.getY0());
    }
    
    @Override
    public void run() {
        this.p1.setCanShoot(false);
        
        while(this.p1.getY() <= this.getHeight() && this.status > 0){
            try {
                
                this.p1.update();
                Thread.sleep(20);
                this.repaint();
                
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
        if(this.status <= 0){
            this.status = this.p2.getWidth();
        }
        
        this.p1.setFx(Collections.max(this.p1.getXs()) / 2);
        this.p1.setFy(Collections.min(this.p1.getYs()));
        
        this.p1.clearVariables();
        this.repaint();
        this.p1.setCanShoot(true);
    }

    /**
     * @return the p1
     */
    public Player getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(Player p1) {
        this.p1 = p1;
    }
    
}
