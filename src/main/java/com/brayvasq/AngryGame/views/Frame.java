/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brayvasq.AngryGame.views;

import com.brayvasq.AngryGame.models.Player;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author brayvasq
 */
public class Frame extends JFrame{
    
    private Panel panel;
    private JPanel acctions;
    private Thread t;
    private JTextField txtAngle, txtSpeed;
    private JLabel lblAngle, lblSpeed;
    private JButton btnShoot;
    
    public Frame(){
        // Set window size
        this.setSize(new Dimension(800,400));
        // Allow to close the window
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // To center the window in the screen
        this.setLocationRelativeTo(null);
        // Setup panel actions
        this.configActions();
        // Setup panel game
        this.configPanel();     
        // To not allow resize the window
        this.setResizable(false);
        // To show the window
        this.setVisible(true);
    }
    
    public void configPanel(){
        this.panel = new Panel();
        this.add(this.panel, BorderLayout.CENTER);
        this.t = new Thread(this.panel);
    }
    
    public void configActions(){
        this.acctions = new JPanel();
        
        this.btnShoot = new JButton("Shoot");
        this.btnShoot.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    int angle = Integer.parseInt(txtAngle.getText());
                    int speed = Integer.parseInt(txtSpeed.getText());
                    
                    if(angle > 90 || angle <= 0){
                        JOptionPane.showMessageDialog(null, "Angle Error: should be > 0 and <= 90");
                    }else if(speed <= 0) {
                        JOptionPane.showMessageDialog(null, "Speed Error: should be > 0");
                    }else {
                        panel.changeValues(angle, speed);
                        panel.shoot();
                    }
                } catch(java.lang.NumberFormatException e){
                    System.out.println(e.toString());
                    JOptionPane.showMessageDialog(null, "Speed and Angle cannot be empty");
                } catch(NullPointerException e){
                    System.out.println(e.toString());
                }
                
            } 
        });
        
        this.txtAngle = new JTextField("");
        this.txtSpeed = new JTextField("");
        
        this.lblAngle = new JLabel("Angle");
        this.lblSpeed = new JLabel("Speed");
        
        this.txtAngle.setPreferredSize(new Dimension( 50, 25 ));
        this.txtSpeed.setPreferredSize(new Dimension( 50, 25 ));
        
        this.acctions.add(this.btnShoot, BorderLayout.EAST);
        this.acctions.add(this.lblAngle, BorderLayout.WEST);
        this.acctions.add(this.txtAngle, BorderLayout.WEST);
        this.acctions.add(this.lblSpeed, BorderLayout.WEST);
        this.acctions.add(this.txtSpeed, BorderLayout.WEST);
        
        this.add(this.acctions, BorderLayout.NORTH);
    }
}
